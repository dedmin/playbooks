# Ansible playbook

[![Pipeline Status](https://gitlab.com/dedmin/playbooks/badges/master/pipeline.svg)](https://gitlab.com/dedmin/playbooks/commits/master)

# Introduction

[Ansible][ansible] roles that can be used to manage [Debian][debian]/[Ubuntu][ubuntu]/[RouterOS][RouterOS] hosts. 

# Ansible

[Ansible][ansible] is an open-source software provisioning, configuration management, and application-deployment tool enabling infrastructure as code.

# Requirements

install requirements:
``` bash
pip3 install --requirement ./requirements
```

# Configuration

Run Ansible playbook:

``` bash
ansible-playbook  playbook.yml -i <yours_environment> --tags 'some_tag' -b
```

# Environment

The default [environment][Environment] is [here][env] or [here][net], for [RouterOS][RouterOS] hosts


[ansible]: https://www.ansible.com
[debian]: https://www.debian.org
[ubuntu]: https://www.ubuntu.com
[RouterOS]: https://mikrotik.com
[Environment]: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
[env]: https://gitlab.com/dedmin/playbooks/-/tree/master/inventory
[net]: https://gitlab.com/dedmin/playbooks/-/tree/master/net